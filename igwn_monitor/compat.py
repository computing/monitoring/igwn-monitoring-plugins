# Copyright (C) Cardiff University (2023)
# SPDX-License-Identifier: MIT

"""Compatibility shims for old Python.
"""

try:
    from contextlib import nullcontext
except ImportError:  # python < 3.7
    from contextlib import contextmanager

    @contextmanager
    def nullcontext(enter_result=None):
        yield enter_result

try:
    from datetime import UTC
except ImportError:  # python < 3.11
    from datetime import timezone
    UTC = timezone.utc
