.. automodapi:: igwn_monitor.http
    :skip: HTTPECPAuth
    :skip: HTTPSciTokenAuth
    :skip: HTTPSPNEGOAuth
    :skip: PreparedRequest
    :skip: Session
    :skip: IgwnAuthSessionMixin
    :skip: igwn_auth_get
    :skip: nullcontext
    :skip: parse_url
    :skip: partial
    :skip: target_audience
    :skip: urlunparse
