##################
`igwn_monitor` API
##################

.. automodapi:: igwn_monitor

.. toctree::
    :caption: Modules
    :maxdepth: 1

    auth
    cli
    http
    logging
    utils
